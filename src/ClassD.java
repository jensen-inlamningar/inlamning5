 public class ClassD {


    private int x, y, z;

    public ClassD(int x, int y, int z) {

        this.x = x; //1

        this.y = y; //2

        this.z = z; //3

    }

    // 10 och 10
    public void modify(int x , int y) {

        // z = 20
        z = x + y;

        // x = 20
        this.x = z;

        // y = 30
        this.y = z + x;

    }


    public void printout() {

        System.out.println("x = " + x);

        System.out.println("y = " + y);

        System.out.println("z = " + z);

    }

}