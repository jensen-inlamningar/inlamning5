public class Dummy {


    public static String name;


    public Dummy( String name ) {

        this.name = name;

    }

    public void skriv() {

        System.out.println( "The name is: " + name );

    }

    public String toString() {

        return name;

    }

}
