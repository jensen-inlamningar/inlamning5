public class Human {

    private String name;

    //här deklarerar jag en instansmetod
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //3 en konstruktor är till för att initiera objekt från en klass.
    //om du inte skapar en klasskonstruktor så skapar java en åt dig men då
    //kan man inte sätta initiala värden till objektet
    public Human(String name) {
        this.name = name;
    }

    public void shoots(){
        System.out.println("BOOOOM");
    }
}
