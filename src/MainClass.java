public class MainClass {



    public static void main( String[] args) {

        ClassD cd = new ClassD(1, 2, 3);

        cd.modify(10, 10);

        cd.printout();

        //5 utskriften blir x = 20 , y = 30 , z = 20, och det är för att modify overridar dom initiala värdet som sätts
        //när klassobjektet skapats, sedan är det bara enkel uträkning vid modify metoden
    }

}