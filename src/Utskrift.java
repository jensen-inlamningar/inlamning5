public class Utskrift {



    public static void main( String[] a) {

        Dummy kk1 = new Dummy( "Ettan" );

        kk1.skriv();

        Dummy kk2 = new Dummy( "Tvåan" );

        kk2.skriv();

        System.out.println( kk1.name.hashCode() + " " + kk2.name.hashCode() );

    }
    //6 utskriften är The name is: Ettan
    //The name is: Tv?an
    //Tv?an Tv?an
    //detta är pga att man använder sig utav keywordet static vilket gör att när man ropar på objektet så
    //pekar man på samma instans vilket gör att utskriften utan metoden "skriv" blir densamma.

}
