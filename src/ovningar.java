public class ovningar {
    public static void main(String[] args) {

        //1

        Human human = new Human("Anton");

        //instansmetoder är metoder som finns inne i en klass som du kommer åt
        //genom att skapa ett object av klassen
        System.out.println(human.getName());

        //2 public kan man komma åt överallt i sitt program(om du har en publik instans
        // eller genom keywordet static), private kommer du bara åt i klassen där den
        //är deklarerad, protected kan man komma åt i det egna paketet som det finns i eller
        //genom arv


        //4 om en child class har en likadan metod som en parent class så är det method overriding
        human.shoots();
        Hunting hunting = new Hunting("Anton");
        hunting.shoots();
    }
}
